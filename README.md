# login_ease



Useful for one-step logging in when using GlobalProtect as a VPN and having a .sh file for logging into AWS.

2 Files:  
- One file contains AppleScript for easy logging into a VPN. (gp.scpt).  
  - Note: the "delay" is important here.  Best to leave them alone.  Your mileage may vary as it's about how responsive the UI is on your box.  
  - The last delay (20 seconds) is important.  It allows time for you to handle MFA and for GP to handle backend processes and get you set up with your SSO so aws script doesn't fail.
- One file contains a bash script that can be dropped into the Mac Dock and triggered with one click (login_ease). 
  - Note that this file calls the AWS script file that you may already have.  The AWS login script is out of scope for this project.  

Use chmod +x filename to make these files executable. 
Put the files in the same directory   
- (Don't forget that your aws login script needs to be accessable either by being in the same directory or by being referenced by path in the login_ease file).  
Use the Mac Finder to drag the login_ease file (NOT the gp.scpt file) to the right-hand side of the Mac's Dock and drop it onto the Dock.  

Click it and enjoy "login-ease"!  