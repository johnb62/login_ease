tell application "System Events" to tell process "GlobalProtect"
    click menu bar item 1 of menu bar 2
    click button 2 of window 1
    delay 2
    tell application "System Events"
        delay .02
        keystroke "username"
        delay 0.2
        key code 48 -- press tab key
        keystroke "password"
        delay 0.2
    end tell
    click button 2 of window 1
    delay 20 (* to allow time to handle MFA and for GlobalProtect to "register" before running the aws login script. Your mileage may vary*)
    click menu bar item 1 of menu bar 2
end tell